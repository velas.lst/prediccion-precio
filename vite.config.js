import { defineConfig } from 'vite';

export default defineConfig({
  server: {
    proxy: {
      '/aml': {
        target: 'https://ussouthcentral.services.azureml.net', // Cambia esto a la URL de tu servidor de desarrollo
        secure: true,
        logLevel: "debug",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    },
  },
});