export default defineEventHandler(async (event) => {
    const runtimeConfig  = (useRuntimeConfig());
    const body = await readBody(event);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    myHeaders.append("Authorization", `Bearer ${runtimeConfig.apiSecret}`);
    try {
        let response = await $fetch(runtimeConfig.public.apiBase, {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify({
                Inputs: {
                    input1: [
                        {
                            "Sistema operativo": body.sistemaOperativo,
                            "Marca": body.marca,
                            "Procesador": body.procesador,
                            "Ram": body.ram,
                            "Almacenamiento": body.almacenamiento,
                            "Resolución de pantalla": body.resolucion,
                            "Camara": 8,
                            "Bateria": body.bateria,
                            "Conectividad":"4G LTE",
                            "Bluethood":"Si",
                            "Gps":"Si",
                            "Sensor":"Acelerómetro",
                            "Sim":"Dual",
                            "Agua":"Si",
                            "Seguridad": body.seguridad,
                            "Precio":0.0
                        }
                    ],
                    // GlobalParameters: {}
                }
            }),
            redirect: 'follow'
        });
        return response;        
    } catch (error) {
        return error;
    }
});