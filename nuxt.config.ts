// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	ssr: true,
	devtools: { enabled: true },
	css: [
		'vuetify/styles/main.sass'
	],
	build: {
		transpile: ["vuetify"]
	},
	runtimeConfig: {
		apiSecret: 'frH/hn5YwfY8aTr8d5xKaIn5lrP71sWX5+66YbvImDl4ldx/xGPeSe87XHvR+tnuFXr//ZKIDYEa+AMCI+wBwA==',
		public: {
			apiBase: 'https://ussouthcentral.services.azureml.net/workspaces/ffd500be878c4ce29a3f66fd49f0c48e/services/60fd3b7dcf804d9b91c44ae87bb26f62/execute?api-version=2.0&format=swagger'
		},
	},
	routeRules: {
		'/aml/**': { proxy: { to: "https://ussouthcentral.services.azureml.net/**" } }
	}
})
